module Intentions exposing
    ( Action(..)
    , Intention
    , Intentions
    , calculate
    )

import Basics.Extra exposing (flip)
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import EntityModels
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Settings exposing (Settings)
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)
import Stats
import Vector2d exposing (Vector2d)
import World exposing (Entities, Entity(..), World)


type alias Intentions =
    Dict Int Intention


type alias Intention =
    { attraction : Vector2d
    , action : Action
    }


type Action
    = Idle
    | Move Direction2d
    | Consume Int
    | Mate Int
    | Hatch
    | Germinate
    | ScatterSeed


type alias Stimulus =
    { attraction : Vector2d
    , eat : Maybe Int
    , mate : Maybe Int
    }


indifferent : Stimulus
indifferent =
    { attraction = Vector2d.zero
    , eat = Nothing
    , mate = Nothing
    }


foldStimuli : Stimulus -> Stimulus -> Stimulus
foldStimuli input output =
    { output
        | attraction =
            Vector2d.sum input.attraction output.attraction
        , eat =
            Maybe.or input.eat output.eat
        , mate =
            Maybe.or input.mate output.mate
    }



-- Plan


calculate : World -> Intentions
calculate world =
    world.entities
        |> Dict.map (calculateEach world)


calculateEach : World -> Int -> Entity -> Intention
calculateEach world id entity =
    let
        habitat =
            ids
                |> List.map (\key -> Dict.get key world.entities)
                |> Maybe.values
                |> List.map2 Tuple.pair ids
                |> Dict.fromList
                |> Dict.remove id

        ids =
            entity
                |> World.region
                |> World.neighbouringRegions 3
                |> List.map (\key -> Dict.get key world.index)
                |> List.map (Maybe.withDefault [])
                |> List.concat
    in
    case entity of
        Seed this ->
            seed world.settings id this habitat

        Plant this ->
            plant world.settings id this habitat

        Egg this ->
            egg world.settings id this habitat

        Herbivore this ->
            herbivore world.settings id this habitat

        Carnivore this ->
            carnivore world.settings id this habitat

        Carcass this ->
            carcass world.settings id this habitat


seed : Settings -> Int -> EntityModels.Seed -> Entities -> Intention
seed settings id this entities =
    let
        attraction : Entity -> Vector2d
        attraction entity =
            case entity of
                Seed other ->
                    flip gravitation
                        this.position
                        other.position

                Plant other ->
                    flip gravitation
                        this.position
                        other.position

                _ ->
                    Vector2d.zero

        {- Put a soft limit on plants population growth.

           In addition to normal attraction for seeds, also make them attracted toward the origin. The atraction is directly proportional to the distance. In effect, seeds will avoid places far away from the origin. It's lika an artificial limit on availabel area of growth.

           Together with plants germination inhibition in crowded habitats (see plant), it will put a soft limit on the population.
        -}
        centripetalForce : Vector2d
        centripetalForce =
            Vector2d.from this.position Point2d.origin
                |> Vector2d.scaleBy settings.seedsCentripetalAttraction

        vegetation =
            entities
                |> Stats.gather
                |> .plants
    in
    { attraction =
        entities
            |> Dict.values
            |> List.map attraction
            |> List.foldl Vector2d.sum centripetalForce
    , action =
        if Spring.atRest this.speed then
            if
                (vegetation > settings.germinationVegetationLimit)
                    && (modBy
                            (round settings.germinationVegetationLimit)
                            (id + Dict.size entities)
                            == 0
                       )
            then
                Idle

            else
                Germinate

        else
            this.attraction
                |> Spring2d.value
                |> Vector2d.direction
                |> Maybe.map Move
                |> Maybe.withDefault Idle
    }


plant : Settings -> Int -> EntityModels.Plant -> Entities -> Intention
plant settings id this entities =
    let
        vegetation =
            entities
                |> Stats.gather
                |> .plants
    in
    if
        (this.succulence > settings.seedingMinSucculence)
            && (vegetation < settings.seedingVegetationLimit)
    then
        { attraction = Vector2d.zero
        , action = ScatterSeed
        }

    else
        { attraction = Vector2d.zero
        , action = Idle
        }


egg : Settings -> Int -> EntityModels.Egg -> Entities -> Intention
egg settings id this entities =
    if this.development > 1 then
        { attraction = Vector2d.zero
        , action = Hatch
        }

    else
        { attraction = Vector2d.zero
        , action = Idle
        }


herbivore :
    Settings
    -> Int
    -> EntityModels.Herbivore
    -> Entities
    -> Intention
herbivore settings id this entities =
    let
        stimulation :
            Int
            -> Entity
            -> Stimulus
        stimulation otherId entity =
            case entity of
                Plant other ->
                    if Point2d.distanceFrom this.position other.position < 5 then
                        { attraction = Vector2d.zero
                        , eat = Just otherId
                        , mate =
                            if this.nutrition > settings.herbivoreOvulationMinNutrition then
                                Just otherId

                            else
                                Nothing
                        }

                    else
                        { attraction =
                            gravitation this.position other.position
                                |> Vector2d.scaleBy
                                    (settings.herbivoreFoodAttraction
                                        * (this.mass - this.nutrition)
                                        * clamp 0.001 2 (other.succulence - this.nutrition)
                                    )
                                |> resetUnsafeVector
                        , eat = Nothing
                        , mate = Nothing
                        }

                Herbivore other ->
                    { attraction =
                        flip gravitation this.position other.position
                            |> Vector2d.scaleBy
                                (other.mass * settings.herbivoreMutualRepulsion)
                            |> Vector2d.scaleBy
                                (this.nutrition ^ 2)
                    , eat = Nothing
                    , mate = Nothing
                    }

                Egg other ->
                    { attraction =
                        flip gravitation this.position other.position
                            |> Vector2d.scaleBy
                                (settings.herbivoreEggRepulsion * this.nutrition)
                    , eat = Nothing
                    , mate = Nothing
                    }

                Carnivore other ->
                    { attraction =
                        flip gravitation this.position other.position
                            |> Vector2d.scaleBy settings.herbivoreCarnivoreRepulsion
                    , eat = Nothing
                    , mate = Nothing
                    }

                _ ->
                    indifferent

        decide : List Stimulus -> Intention
        decide stimuli =
            let
                stimulus =
                    List.foldl foldStimuli indifferent stimuli

                attraction =
                    Spring2d.value this.attraction
            in
            case ( stimulus.attraction, stimulus.eat, stimulus.mate ) of
                ( _, _, Just mateId ) ->
                    { attraction = stimulus.attraction
                    , action =
                        Mate mateId
                    }

                ( _, Just foodId, _ ) ->
                    { attraction = stimulus.attraction
                    , action =
                        if Vector2d.length attraction > settings.herbivoreConsumptionThershold then
                            attraction
                                |> Vector2d.direction
                                |> Maybe.map Move
                                |> Maybe.withDefault Idle

                        else if this.nutrition > this.mass then
                            Idle

                        else
                            Consume foodId
                    }

                _ ->
                    { attraction = stimulus.attraction
                    , action =
                        if Vector2d.length attraction > settings.herbivoreIdleThershold then
                            attraction
                                |> Vector2d.direction
                                |> Maybe.map Move
                                |> Maybe.withDefault Idle

                        else
                            Idle
                    }
    in
    entities
        |> Dict.map stimulation
        |> Dict.values
        |> decide


carnivore :
    Settings
    -> Int
    -> EntityModels.Carnivore
    -> Entities
    -> Intention
carnivore settings id this entities =
    let
        stimulation :
            Int
            -> Entity
            -> Stimulus
        stimulation otherId entity =
            case entity of
                Carcass other ->
                    if Point2d.distanceFrom this.position other.position < max 2 (2 * (other.mass + this.mass)) then
                        { attraction = Vector2d.zero
                        , eat = Just otherId
                        , mate = Nothing
                        }

                    else
                        { attraction =
                            gravitation this.position other.position
                                |> Vector2d.scaleBy
                                    (settings.carnivoreCarcassAttraction
                                        * other.mass
                                        / clamp 0.001 1 this.nutrition
                                    )
                                |> resetUnsafeVector
                        , eat = Nothing
                        , mate = Nothing
                        }

                Seed other ->
                    { attraction =
                        gravitation this.position other.position
                            |> Vector2d.scaleBy
                                (settings.carnivoreSeedAttraction
                                    * Spring.value other.speed
                                    * this.nutrition
                                )
                            |> resetUnsafeVector
                    , eat = Nothing
                    , mate = Nothing
                    }

                Herbivore other ->
                    if Point2d.distanceFrom this.position other.position < 5 then
                        { attraction = Vector2d.zero
                        , eat = Just otherId
                        , mate = Nothing
                        }

                    else
                        { attraction =
                            gravitation this.position other.position
                                |> Vector2d.scaleBy
                                    (settings.carnivorePreyAttraction
                                        * this.nutrition
                                        * clamp 0.001 4 other.mass
                                    )
                                |> Vector2d.scaleBy
                                    (this.pregnancy
                                        |> Maybe.map ((*) settings.carnivorePregnancyHuntingBoost)
                                        |> Maybe.withDefault 1
                                    )
                                |> resetUnsafeVector
                        , eat = Nothing
                        , mate = Nothing
                        }

                Carnivore other ->
                    if
                        this.pregnancy
                            == Nothing
                            && other.pregnancy
                            == Nothing
                            && this.nutrition
                            > settings.carnivoreMatingMinNutrition
                            && other.mass
                            > settings.carnivoreMaturityMinMass
                    then
                        -- Adult feels sexual attraction when they are nourished
                        if Point2d.distanceFrom this.position other.position < 5 then
                            { attraction = Vector2d.zero
                            , eat = Nothing
                            , mate = Just otherId
                            }

                        else
                            { attraction =
                                gravitation this.position other.position
                                    |> Vector2d.scaleBy
                                        (settings.carnivoreSexualAttraction
                                            * this.nutrition
                                        )
                            , eat = Nothing
                            , mate = Nothing
                            }

                    else
                        -- Children and hungry adults - no sexual attraction
                        { attraction =
                            gravitation other.position this.position
                                |> Vector2d.scaleBy
                                    (settings.carnivoreMutualRepulsion
                                        * other.mass
                                    )
                        , eat = Nothing
                        , mate = Nothing
                        }

                _ ->
                    indifferent

        decide : List Stimulus -> Intention
        decide stimuli =
            let
                stimulus =
                    List.foldl foldStimuli indifferent stimuli

                attraction =
                    Spring2d.value this.attraction
            in
            case ( stimulus.attraction, stimulus.eat, stimulus.mate ) of
                ( _, _, Just mateId ) ->
                    { attraction = stimulus.attraction
                    , action =
                        Mate mateId
                    }

                ( _, Just foodId, _ ) ->
                    { attraction = stimulus.attraction
                    , action =
                        if Vector2d.length attraction > settings.carnivoreConsumptionThreshold then
                            attraction
                                |> Vector2d.direction
                                |> Maybe.map Move
                                |> Maybe.withDefault Idle

                        else if this.nutrition > this.mass then
                            Idle

                        else
                            Consume foodId
                    }

                _ ->
                    { attraction = stimulus.attraction
                    , action =
                        if Vector2d.length attraction > settings.carnivoreIdleThreshold then
                            attraction
                                |> Vector2d.direction
                                |> Maybe.map Move
                                |> Maybe.withDefault Idle

                        else
                            Idle
                    }
    in
    entities
        |> Dict.map stimulation
        |> Dict.values
        |> decide


carcass : Settings -> Int -> EntityModels.Carcass -> Entities -> Intention
carcass settings id this entities =
    { action = Idle
    , attraction = Vector2d.zero
    }


gravitation : Point2d -> Point2d -> Vector2d
gravitation attractor object =
    let
        distance =
            Point2d.distanceFrom attractor object / 100

        maxVisibility =
            1.5

        visibility =
            maxVisibility
                |> (/) (maxVisibility - distance)
                |> max 0

        force =
            distance
                |> (/) visibility
                |> min 10000
    in
    object
        |> Direction2d.from attractor
        |> Maybe.map (Vector2d.withLength force)
        |> Maybe.withDefault Vector2d.zero


resetUnsafeVector : Vector2d -> Vector2d
resetUnsafeVector v =
    if
        (v |> Vector2d.xComponent |> isNaN)
            || (v |> Vector2d.yComponent |> isNaN)
            || (v |> Vector2d.xComponent |> isInfinite)
            || (v |> Vector2d.yComponent |> isInfinite)
    then
        Vector2d.zero

    else
        v
