import { Elm  } from "./IntentionsWorker.elm"

const program = Elm.IntentionsWorker

// A hack for `ReferenceError: document is not defined` in development build.
// TODO: Investigate more and file an issue with Elm or Parcel.
self.document = { body: null }

const worker = Elm.IntentionsWorker.init()


let next_batch = null;
let busy = false;

worker.ports.intentionsOut.subscribe((data) => {
  // console.count("Worker done.")
  postMessage(data);

  if (next_batch === null) {
    // console.count("Nothing to do atm.");
    busy = false;
  }
  else { 
    // console.count("Taking next batch.")
    worker.ports.worldIn.send(next_batch)
    next_batch = null
    busy = true
  }
})

onmessage = (event) => {
  // console.count("New batch received.");

  if (busy) {
    // console.count("Worker is already busy.")
    next_batch = event.data
  }
  else {
    // console.count("Kicking the worker.")
    next_batch = null
    busy = true
    worker.ports.worldIn.send(event.data)
  }
};

worker.ports.errorsOut.subscribe(({ message, details }) => {
  console.error(message, details)
})

console.log("Worker ready");
