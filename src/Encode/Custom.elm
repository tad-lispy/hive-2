module Encode.Custom exposing
    ( carcass
    , carnivore
    , egg
    , entities
    , entity
    , herbivore
    , intentions
    , plant
    , point2d
    , seed
    , spring
    , spring2d
    , vector2d
    , world
    )

import Direction2d
import EntityModels
import Intentions exposing (Action(..), Intention, Intentions)
import Json.Encode exposing (..)
import Point2d exposing (Point2d)
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)
import Vector2d exposing (Vector2d)
import World exposing (Entities, Entity(..), World)


world : World -> Value
world data =
    object
        [ ( "seed", int data.seed )
        , ( "entities", entities data.entities )
        ]


intentions : Intentions -> Value
intentions data =
    data |> dict String.fromInt intention


entities : Entities -> Value
entities data =
    data |> dict String.fromInt entity


intention : Intention -> Value
intention data =
    object
        [ ( "attraction", data.attraction |> vector2d )
        , ( "action", data.action |> action )
        ]


action : Action -> Value
action data =
    case data of
        Idle ->
            object [ ( "tag", "Idle" |> string ) ]

        Move direction ->
            object
                [ ( "tag", "Move" |> string )
                , ( "direction"
                  , direction
                        |> Direction2d.toAngle
                        |> float
                  )
                ]

        Consume id ->
            object
                [ ( "tag", "Consume" |> string )
                , ( "id", id |> int )
                ]

        Mate id ->
            object
                [ ( "tag", "Mate" |> string )
                , ( "id", id |> int )
                ]

        Hatch ->
            object [ ( "tag", "Hatch" |> string ) ]

        Germinate ->
            object [ ( "tag", "Germinate" |> string ) ]

        ScatterSeed ->
            object [ ( "tag", "ScatterSeed" |> string ) ]


entity : Entity -> Value
entity data =
    case data of
        Seed this ->
            seed this

        Plant this ->
            plant this

        Egg this ->
            egg this

        Herbivore this ->
            herbivore this

        Carnivore this ->
            carnivore this

        Carcass this ->
            carcass this


seed : EntityModels.Seed -> Value
seed this =
    object
        [ ( "tag", "Seed" |> string )
        , ( "position", this.position |> point2d )
        , ( "speed", this.speed |> spring )
        , ( "attraction", this.attraction |> spring2d )
        ]


plant : EntityModels.Plant -> Value
plant this =
    object
        [ ( "tag", "Plant" |> string )
        , ( "position", this.position |> point2d )
        , ( "succulence", this.succulence |> float )
        ]


egg : EntityModels.Egg -> Value
egg this =
    object
        [ ( "tag", "Egg" |> string )
        , ( "position", this.position |> point2d )
        , ( "development", this.development |> float )
        ]


herbivore : EntityModels.Herbivore -> Value
herbivore this =
    object
        [ ( "tag", "Herbivore" |> string )
        , ( "position", this.position |> point2d )
        , ( "mass", this.mass |> float )
        , ( "nutrition", this.nutrition |> float )
        , ( "attraction", this.attraction |> spring2d )
        ]


carnivore : EntityModels.Carnivore -> Value
carnivore this =
    object
        [ ( "tag", "Carnivore" |> string )
        , ( "position", this.position |> point2d )
        , ( "mass", this.mass |> float )
        , ( "nutrition", this.nutrition |> float )
        , ( "pregnancy"
          , this.pregnancy
                |> Maybe.map float
                |> Maybe.withDefault null
          )
        , ( "attraction", this.attraction |> spring2d )
        ]


carcass : EntityModels.Carcass -> Value
carcass this =
    object
        [ ( "tag", "Carcass" |> string )
        , ( "position", this.position |> point2d )
        , ( "mass", this.mass |> float )
        ]


point2d : Point2d -> Value
point2d data =
    object
        [ ( "x", data |> Point2d.xCoordinate |> float )
        , ( "y", data |> Point2d.yCoordinate |> float )
        ]


spring2d : Spring2d -> Value
spring2d data =
    object
        [ ( "target", data |> Spring2d.target |> vector2d )
        , ( "value", data |> Spring2d.value |> vector2d )
        ]


spring : Spring -> Value
spring data =
    object
        [ ( "target", data |> Spring.target |> float )
        , ( "value", data |> Spring.value |> float )
        ]


vector2d : Vector2d -> Value
vector2d data =
    object
        [ ( "x", data |> Vector2d.xComponent |> float )
        , ( "y", data |> Vector2d.yComponent |> float )
        ]
