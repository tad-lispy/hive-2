module EntityModels exposing
    ( Carcass
    , Carnivore
    , Egg
    , Herbivore
    , Plant
    , Seed
    )

import Point2d exposing (Point2d)
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)


type alias Seed =
    { position : Point2d
    , attraction : Spring2d
    , speed : Spring
    }


type alias Plant =
    { position : Point2d
    , succulence : Float
    }


type alias Egg =
    { position : Point2d
    , development : Float
    }


type alias Herbivore =
    { position : Point2d
    , mass : Float
    , nutrition : Float
    , attraction : Spring2d
    }


type alias Carnivore =
    { position : Point2d
    , mass : Float
    , nutrition : Float
    , pregnancy : Maybe Float
    , attraction : Spring2d
    }


type alias Carcass =
    { position : Point2d
    , mass : Float
    }
