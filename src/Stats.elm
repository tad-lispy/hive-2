module Stats exposing
    ( DataPoint
    , DataPoints
    , Stats
    , appendDataPoints
    , empty
    , gather
    )

import Dict exposing (Dict)
import World exposing (Entities, Entity(..))


type alias DataPoints =
    { plants : List DataPoint
    , herbivores : List DataPoint
    , carnivores : List DataPoint
    }


type alias DataPoint =
    { time : Float
    , quantity : Float
    }


type alias Stats =
    { plants : Float
    , herbivores : Float
    , carnivores : Float
    }


empty : DataPoints
empty =
    { plants = []
    , herbivores = []
    , carnivores = []
    }


gather : Entities -> Stats
gather entities =
    let
        countEntities : Int -> Entity -> Stats -> Stats
        countEntities id entity stats =
            case entity of
                Seed this ->
                    stats

                Plant this ->
                    { stats | plants = stats.plants + 1 }

                Egg this ->
                    stats

                Herbivore this ->
                    { stats | herbivores = stats.herbivores + 1 }

                Carnivore this ->
                    { stats | carnivores = stats.carnivores + 1 }

                Carcass this ->
                    stats
    in
    entities
        |> Dict.foldl countEntities
            { plants = 0
            , herbivores = 0
            , carnivores = 0
            }


appendDataPoints : Float -> Stats -> DataPoints -> DataPoints
appendDataPoints time stats dataPoints =
    let
        plants =
            stats.plants
                |> logBase 10
                |> max 0
                |> DataPoint time

        herbivores =
            stats.herbivores
                |> logBase 10
                |> max 0
                |> DataPoint time

        carnivores =
            stats.carnivores
                |> logBase 10
                |> max 0
                |> DataPoint time
    in
    { plants = plants :: dataPoints.plants
    , herbivores = herbivores :: dataPoints.herbivores
    , carnivores = carnivores :: dataPoints.carnivores
    }
