module Main exposing (main)

import Axis2d
import Basics.Extra exposing (flip)
import BoundingBox2d
import Browser
import Browser.Events
import Color
import Color.Generator as Color
import Decode.Custom as Decode
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import EntityModels
import Frame2d
import Geometry.Svg
import Html exposing (Html)
import Html.Attributes
import Intentions exposing (Action(..), Intention, Intentions)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import LineChart
import LineChart.Area
import LineChart.Axis
import LineChart.Axis.Intersection
import LineChart.Colors
import LineChart.Container
import LineChart.Dots
import LineChart.Events
import LineChart.Grid
import LineChart.Interpolation
import LineChart.Junk
import LineChart.Legends
import LineChart.Line
import List.Extra as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Ports
import Rectangle2d
import Settings exposing (Settings)
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)
import Stats exposing (DataPoint, DataPoints, Stats)
import Svg exposing (Svg)
import Svg.Attributes
import Svg.Events
import Time
import Transformations
import Vector2d exposing (Vector2d)
import World exposing (Entities, Entity(..), World)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { world : Decode.Value
    }


type alias Model =
    { world : World
    , intentions : Intentions
    , track : Maybe Int
    , play : Bool
    , tab : Tab
    , showRegions : Bool
    , focus : Spring2d
    , zoom : Spring
    , stats : Stats
    , dataPoints : Stats.DataPoints
    , clock : Float
    , settings : Settings
    }


type Msg
    = Animate Float
    | IntentionsUpdate (Result Decode.Error Intentions)
    | GatherStats
    | RecordDataPoints
    | Track (Maybe Int)
    | Play
    | Pause
    | Show Tab
    | Zoom Float
    | Pan Direction2d
    | ShowRegions
    | HideRegions
    | Reset
    | ApplySettings Settings
    | UpdateSettings Settings.Msg


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        world =
            flags.world
                |> Decode.decodeValue Decode.world
                |> Result.toMaybe
                |> Maybe.withDefault
                    (World.empty
                        |> World.populateCarnivores 10
                        |> World.populateHerbivores 50
                        |> World.populatePlants 50
                    )
    in
    ( { track = Nothing
      , play = True
      , tab = SceneTab
      , showRegions = False
      , zoom =
            { strength = 50, dampness = 5 }
                |> Spring.create
                |> Spring.jumpTo 10
                |> Spring.setTarget 10
      , focus = Spring2d.create { strength = 50, dampness = 5 }
      , clock = 0
      , stats =
            { plants = 0
            , herbivores = 0
            , carnivores = 0
            }
      , dataPoints =
            Stats.empty
                |> Stats.appendDataPoints 0 (Stats.gather world.entities)
      , world = world
      , intentions =
            Intentions.calculate world
      , settings = world.settings
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        playSubscriptions : Sub Msg
        playSubscriptions =
            if model.play then
                [ Time.every 1000
                    -- every second
                    (always GatherStats)
                , Time.every 60000
                    -- every minute
                    (always RecordDataPoints)
                , Browser.Events.onAnimationFrameDelta Animate
                , Ports.intentions IntentionsUpdate
                ]
                    |> Sub.batch

            else
                Sub.none

        keyPressDecoder : Decoder Msg
        keyPressDecoder =
            Decode.field "key" Decode.string
                |> Decode.andThen
                    (\key ->
                        case key of
                            " " ->
                                if model.play then
                                    Decode.succeed Pause

                                else
                                    Decode.succeed Play

                            "=" ->
                                Decode.succeed (Zoom 1)

                            "-" ->
                                Decode.succeed (Zoom -1)

                            "a" ->
                                Direction2d.x
                                    |> Direction2d.reverse
                                    |> Pan
                                    |> Decode.succeed

                            "s" ->
                                Direction2d.y
                                    |> Pan
                                    |> Decode.succeed

                            "w" ->
                                Direction2d.y
                                    |> Direction2d.reverse
                                    |> Pan
                                    |> Decode.succeed

                            "d" ->
                                Direction2d.x
                                    |> Pan
                                    |> Decode.succeed

                            "r" ->
                                Decode.succeed <|
                                    if model.showRegions then
                                        HideRegions

                                    else
                                        ShowRegions

                            "p" ->
                                Decode.succeed <|
                                    if model.play then
                                        Pause

                                    else
                                        Animate 32

                            "t" ->
                                let
                                    ids =
                                        model.world.entities
                                            |> Dict.filter
                                                (\_ entity ->
                                                    case entity of
                                                        Carnivore _ ->
                                                            True

                                                        _ ->
                                                            False
                                                )
                                            |> Dict.keys

                                    currentId =
                                        Maybe.withDefault 0 model.track

                                    id =
                                        ids
                                            |> List.dropWhile
                                                (\item ->
                                                    item <= currentId
                                                )
                                            |> List.head
                                            |> flip Maybe.or (List.head ids)
                                in
                                Track id
                                    |> Decode.succeed

                            _ ->
                                Decode.fail "Unrecognized key was pressed"
                    )
    in
    Sub.batch
        [ Browser.Events.onKeyPress keyPressDecoder
        , playSubscriptions
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Animate delta ->
            let
                clock =
                    model.clock + virtualDelta

                virtualDelta =
                    min delta limit

                limit =
                    128

                target =
                    case model.track of
                        Just id ->
                            model.world.entities
                                |> Dict.get id
                                |> Maybe.map World.position
                                |> Maybe.map (Vector2d.from Point2d.origin)
                                |> Maybe.withDefault (Spring2d.target model.focus)

                        Nothing ->
                            Spring2d.target model.focus

                world =
                    performAll
                        virtualDelta
                        model.intentions
                        model.world
            in
            ( { model
                | world = world
                , clock = clock
                , zoom =
                    Spring.animate delta model.zoom
                , focus =
                    model.focus
                        |> Spring2d.setTarget target
                        |> Spring2d.animate delta

                -- , intentions =
                -- Intentions.calculate world
              }
            , Ports.sendWorld world
              -- , Cmd.none
            )

        IntentionsUpdate (Ok intentions) ->
            ( { model | intentions = intentions }
            , Cmd.none
            )

        IntentionsUpdate (Err error) ->
            ( model
            , error
                |> Decode.errorToString
                |> Encode.string
                |> Just
                |> Ports.sendError "Error decoding intentions"
            )

        GatherStats ->
            ( { model
                | stats =
                    Stats.gather model.world.entities
              }
            , Ports.sendSnapshot model.world
            )

        RecordDataPoints ->
            ( { model
                | dataPoints =
                    model.dataPoints
                        |> appendDataPoints model.clock model.stats
              }
            , Cmd.none
            )

        Track id ->
            ( { model | track = id }
            , Cmd.none
            )

        Zoom delta ->
            let
                zoom =
                    if model.play then
                        Spring.setTarget target model.zoom

                    else
                        model.zoom
                            |> Spring.jumpTo target
                            |> Spring.setTarget target

                target =
                    model.zoom
                        |> Spring.target
                        |> (+) delta
                        |> Basics.clamp 1 500
            in
            ( { model
                | zoom = zoom
              }
            , Cmd.none
            )

        Pan direction ->
            let
                focus =
                    if model.play then
                        Spring2d.setTarget target model.focus

                    else
                        model.focus
                            |> Spring2d.jumpTo target
                            |> Spring2d.setTarget target

                target =
                    model.focus
                        |> Spring2d.target
                        |> Vector2d.sum translation

                translation =
                    Vector2d.withLength length direction

                length =
                    500 / Spring.value model.zoom
            in
            ( { model
                | focus = focus
                , track = Nothing
              }
            , Cmd.none
            )

        ShowRegions ->
            ( { model | showRegions = True }
            , Cmd.none
            )

        HideRegions ->
            ( { model | showRegions = False }
            , Cmd.none
            )

        Play ->
            ( { model | play = True }
            , Cmd.none
            )

        Pause ->
            ( { model | play = False }
            , Cmd.none
            )

        Show tab ->
            ( { model | tab = tab }
            , Cmd.none
            )

        Reset ->
            ( model
            , Ports.reset
            )

        UpdateSettings m ->
            ( { model | settings = Settings.update m model.settings }
            , Cmd.none
            )

        ApplySettings settings ->
            let
                world =
                    model.world
            in
            ( { model | world = { world | settings = settings } }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    let
        tab =
            case model.tab of
                SceneTab ->
                    sceneUi model

                StatsTab ->
                    statsUi model

                SettingsTab ->
                    Settings.ui
                        { onChange = UpdateSettings
                        , onSubmit = ApplySettings
                        }
                        model.settings
    in
    tab
        |> Element.el
            [ Element.paddingEach
                { top = 60
                , right = 0
                , bottom = 0
                , left = 0
                }
            , Element.width Element.fill
            , Element.height Element.fill
            ]
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            , controlsUi model |> Element.inFront
            ]


appendDataPoints : Float -> Stats -> DataPoints -> DataPoints
appendDataPoints time stats dataPoints =
    let
        plants =
            stats.plants
                |> logBase 10
                |> max 0
                |> DataPoint time

        herbivores =
            stats.herbivores
                |> logBase 10
                |> max 0
                |> DataPoint time

        carnivores =
            stats.carnivores
                |> logBase 10
                |> max 0
                |> DataPoint time
    in
    { plants = plants :: dataPoints.plants
    , herbivores = herbivores :: dataPoints.herbivores
    , carnivores = carnivores :: dataPoints.carnivores
    }



-- Perform


performAll : Float -> Intentions -> World -> World
performAll delta intentions world =
    intentions
        |> Dict.foldl (performEach delta) world


performEach : Float -> Int -> Intention -> World -> World
performEach delta id intention world =
    case Dict.get id world.entities of
        Nothing ->
            -- It's dead. Proceed.
            world

        Just (Seed this) ->
            performSeed delta id intention world this

        Just (Plant this) ->
            performPlant delta id intention world this

        Just (Egg this) ->
            performEgg delta id intention world this

        Just (Herbivore this) ->
            performHerbivore delta id intention world this

        Just (Carnivore this) ->
            performCarnivore delta id intention world this

        Just (Carcass this) ->
            performCarcass delta id intention world this


performSeed : Float -> Int -> Intention -> World -> EntityModels.Seed -> World
performSeed delta id intention world this =
    case intention.action of
        Move direction ->
            let
                speed =
                    this.speed
                        |> Spring.value
                        |> (*) (delta / 1000)

                displacement =
                    Vector2d.withLength speed direction

                updated =
                    Seed
                        { this
                            | position =
                                Point2d.translateBy displacement this.position
                            , attraction =
                                this.attraction
                                    |> Spring2d.setTarget intention.attraction
                                    |> Spring2d.animate delta
                            , speed =
                                Spring.animate delta this.speed
                        }
            in
            World.replace id updated world

        Germinate ->
            World.replace id
                (Plant
                    { position = this.position
                    , succulence = 0
                    }
                )
                world

        _ ->
            world


performPlant :
    Float
    -> Int
    -> Intention
    -> World
    -> EntityModels.Plant
    -> World
performPlant delta id intention world this =
    if this.succulence < 0 then
        World.remove id world

    else
        case intention.action of
            Idle ->
                let
                    growth =
                        world.settings.plantGrowthRate * (delta / 1000)

                    -- / max this.succulence 0.4
                in
                -- if growth > 0.01 then
                world
                    |> World.replace id
                        (Plant
                            { this
                                | succulence =
                                    this.succulence + growth
                            }
                        )

            -- else
            -- Let's save some computation here.
            -- world
            ScatterSeed ->
                if this.succulence < world.settings.seedingMinSucculence then
                    -- This is a safeguard against excessive seeding before the worker calculates new intentions. Probably should be implemented in more elegant fasion.
                    world

                else
                    let
                        updated =
                            Plant
                                { this
                                    | succulence =
                                        this.succulence - world.settings.seedingCost
                                }

                        target =
                            {- Very pseudo random initial target :)

                               The point of this is to avoid seeds stacking in the same point as the parent or always being shoot in the same direction. This should give statisticaly random distribution.
                            -}
                            id
                                |> toFloat
                                |> (+) delta
                                |> Direction2d.fromAngle
                                |> Direction2d.toVector

                        seed =
                            Seed
                                { position = this.position
                                , speed =
                                    { strength = world.settings.seedSpeedStrength
                                    , dampness = world.settings.seedSpeedDampness
                                    }
                                        |> Spring.create
                                        |> Spring.jumpTo world.settings.seedInitialSpeed
                                        |> Spring.setTarget 0
                                , attraction =
                                    { strength = world.settings.seedAttractionStrength
                                    , dampness = world.settings.seedAttractionDampness
                                    }
                                        |> Spring2d.create
                                        |> Spring2d.jumpTo target
                                }
                    in
                    world
                        |> World.replace id updated
                        |> World.insert seed

            _ ->
                world


performEgg :
    Float
    -> Int
    -> Intention
    -> World
    -> EntityModels.Egg
    -> World
performEgg delta id intention world this =
    case intention.action of
        Idle ->
            let
                progress =
                    world.settings.eggDevelopmentRate * delta / 1000

                updated =
                    Egg { this | development = this.development + progress }
            in
            World.replace id updated world

        Hatch ->
            let
                updated =
                    Herbivore
                        { position = this.position
                        , mass = world.settings.herbivoreBirthMass
                        , nutrition = world.settings.herbivoreBirthNutrition
                        , attraction =
                            { strength = world.settings.herbivoreAttractionStrength
                            , dampness = world.settings.herbivoreAttractionDampness
                            }
                                |> Spring2d.create
                        }
            in
            World.replace id updated world

        _ ->
            world


performHerbivore :
    Float
    -> Int
    -> Intention
    -> World
    -> EntityModels.Herbivore
    -> World
performHerbivore delta id intention world this =
    if this.nutrition < 0 then
        let
            updated =
                Carcass
                    { position = this.position
                    , mass = this.mass
                    }
        in
        World.replace id updated world

    else
        case intention.action of
            Move direction ->
                let
                    expense =
                        world.settings.herbivoreMoveCost
                            * distance
                            * this.mass

                    distance =
                        world.settings.herbivoreMoveSpeed
                            * delta
                            / 1000

                    translation =
                        Vector2d.withLength distance direction

                    updated =
                        Herbivore
                            { this
                                | position =
                                    this.position
                                        |> Point2d.translateBy translation
                                , nutrition =
                                    this.nutrition - expense
                                , attraction =
                                    this.attraction
                                        |> Spring2d.setTarget intention.attraction
                                        |> Spring2d.animate delta
                            }
                in
                World.replace id updated world

            Mate plantId ->
                if this.nutrition < world.settings.herbivoreOvulationMinNutrition then
                    -- Safeguard against excessive egg laying caused by delayed intentions update from the intention worker. Probably shuld be implemented in more elegant manner.
                    world

                else
                    case Dict.get plantId world.entities of
                        Just (Plant other) ->
                            let
                                nest =
                                    Point2d.midpoint this.position other.position
                            in
                            world
                                |> World.insert
                                    (Egg
                                        { position = nest
                                        , development = 0
                                        }
                                    )
                                |> World.replace id
                                    (Herbivore
                                        { this
                                            | nutrition = this.nutrition - world.settings.herbivoreOvulationCost
                                            , attraction =
                                                this.attraction
                                                    |> Spring2d.setTarget intention.attraction
                                                    |> Spring2d.animate delta
                                        }
                                    )

                        _ ->
                            world

            Consume foodId ->
                let
                    food =
                        world.entities
                            |> Dict.get foodId

                    bite =
                        world.settings.herbivoreConsumptionRate
                            * delta
                            / 1000

                    gain =
                        world.settings.herbivoreConsumptionEfficiency
                            * bite
                in
                case food of
                    Just (Plant other) ->
                        world
                            |> World.replace foodId
                                (Plant
                                    { other
                                        | succulence = other.succulence - bite
                                    }
                                )
                            |> World.replace id
                                (Herbivore
                                    { this
                                        | nutrition = this.nutrition + gain
                                        , attraction =
                                            this.attraction
                                                |> Spring2d.setTarget intention.attraction
                                                |> Spring2d.animate delta
                                    }
                                )

                    _ ->
                        world

            Idle ->
                let
                    growth =
                        world.settings.herbivoreGrowthRate
                            * max this.nutrition 0.01
                            * delta
                            / (1000 * this.mass)

                    cost =
                        world.settings.herbivoreGrowthCost
                            * growth
                in
                world
                    |> World.replace id
                        (Herbivore
                            { this
                                | mass = this.mass + growth
                                , nutrition = this.nutrition - cost
                                , attraction =
                                    this.attraction
                                        |> Spring2d.setTarget intention.attraction
                                        |> Spring2d.animate delta
                            }
                        )

            _ ->
                world


performCarnivore :
    Float
    -> Int
    -> Intention
    -> World
    -> EntityModels.Carnivore
    -> World
performCarnivore delta id intention world this =
    if this.nutrition < 0 then
        let
            updated =
                Carcass
                    { position = this.position
                    , mass = this.mass
                    }
        in
        World.replace id updated world

    else if Maybe.withDefault 0 this.pregnancy > 1 then
        let
            birthVector =
                Point2d.origin
                    |> Vector2d.from this.position
                    |> Vector2d.normalize
                    |> Vector2d.sum intention.attraction
                    |> Vector2d.normalize

            birthPlace =
                -- Children are born in the direction of good stuff / away from harm
                Point2d.translateBy birthVector this.position
        in
        world
            |> World.insert
                (Carnivore
                    { position = birthPlace
                    , mass = world.settings.carnivoreBirthMass
                    , nutrition = world.settings.carnivoreBirthNutrition
                    , pregnancy = Nothing
                    , attraction =
                        Spring2d.create
                            { strength =
                                world.settings.carnivoreAttractionStrength
                            , dampness =
                                world.settings.carnivoreAttractionDampness
                            }
                    }
                )
            |> World.replace id
                (Carnivore
                    { this
                        | nutrition =
                            this.nutrition
                                - world.settings.carnivoreBirthCost
                        , pregnancy = Nothing
                    }
                )

    else
        case intention.action of
            Move direction ->
                let
                    translation =
                        Vector2d.withLength distance direction

                    expense =
                        (world.settings.carnivoreMoveCost * distance)
                            + (world.settings.carnivorePregnancyCost
                                * fetalDevelopment
                              )

                    distance =
                        world.settings.carnivoreMoveSpeed
                            * delta
                            / 1000

                    fetalDevelopment =
                        if this.pregnancy == Nothing then
                            0

                        else
                            world.settings.carnivorePregnancyDevelopmentRate
                                * delta
                                / 1000
                in
                world
                    |> World.replace id
                        (Carnivore
                            { this
                                | position =
                                    this.position
                                        |> Point2d.translateBy translation
                                , nutrition =
                                    this.nutrition - expense
                                , attraction =
                                    this.attraction
                                        |> Spring2d.setTarget intention.attraction
                                        |> Spring2d.animate delta
                                , pregnancy =
                                    this.pregnancy
                                        |> Maybe.map ((+) fetalDevelopment)
                            }
                        )

            Mate mateId ->
                case Dict.get mateId world.entities of
                    Just (Carnivore other) ->
                        if this.pregnancy == Nothing && other.pregnancy == Nothing then
                            world
                                |> World.replace id
                                    (Carnivore
                                        { this
                                            | nutrition = this.nutrition - world.settings.carnivoreMatingCost
                                        }
                                    )
                                |> World.replace mateId
                                    (Carnivore
                                        { other
                                            | pregnancy = Just 0
                                        }
                                    )

                        else
                            world

                    _ ->
                        world

            Consume foodId ->
                case Dict.get foodId world.entities of
                    Just (Carcass other) ->
                        let
                            bite =
                                world.settings.carnivoreConsumptionRate
                                    * delta
                                    / 1000

                            gain =
                                world.settings.carnivoreConsumptionEfficiency
                                    * bite
                        in
                        world
                            |> World.replace foodId
                                (Carcass
                                    { other
                                        | mass = other.mass - bite
                                    }
                                )
                            |> World.replace id
                                (Carnivore
                                    { this
                                        | nutrition = this.nutrition + gain
                                    }
                                )

                    Just (Herbivore other) ->
                        world
                            |> World.replace foodId
                                (Herbivore
                                    { other
                                        | nutrition = other.nutrition - world.settings.carnivoreAttackDamage
                                    }
                                )
                            |> World.replace id
                                (Carnivore
                                    { this
                                        | nutrition = this.nutrition - world.settings.carnivoreAttackCost
                                    }
                                )

                    _ ->
                        world

            Idle ->
                let
                    growth =
                        world.settings.carnivoreGrowthRate
                            * max this.nutrition 0.01
                            / (1000 * this.mass)

                    fetalDevelopment =
                        if this.pregnancy == Nothing then
                            0

                        else
                            world.settings.carnivorePregnancyDevelopmentRate
                                * delta
                                / 1000

                    expense =
                        (world.settings.carnivoreGrowthCost * growth)
                            + (world.settings.carnivorePregnancyCost
                                * fetalDevelopment
                              )
                in
                world
                    |> World.replace id
                        (Carnivore
                            { this
                                | mass = this.mass + growth
                                , nutrition = this.nutrition - expense
                                , pregnancy =
                                    this.pregnancy
                                        |> Maybe.map ((+) fetalDevelopment)
                                , attraction =
                                    this.attraction
                                        |> Spring2d.setTarget intention.attraction
                                        |> Spring2d.animate delta
                            }
                        )

            _ ->
                world


performCarcass :
    Float
    -> Int
    -> Intention
    -> World
    -> EntityModels.Carcass
    -> World
performCarcass delta id intention world this =
    if this.mass < 0 then
        world
            |> World.replace id
                (Plant
                    { position = this.position
                    , succulence = 0.0
                    }
                )

    else
        let
            rot =
                world.settings.decompositionRate
                    * delta
                    / 1000
        in
        world
            |> World.replace id
                (Carcass
                    { this
                        | mass = this.mass - rot
                    }
                )



-- UI elements


type Tab
    = SceneTab
    | StatsTab
    | SettingsTab


splashUi : Element Msg
splashUi =
    "Hive 2"
        |> Element.text
        |> Element.el
            [ Element.centerX
            , Element.centerY
            ]


sceneUi : Model -> Element Msg
sceneUi model =
    let
        width =
            8000 / Spring.value model.zoom

        height =
            8000 / Spring.value model.zoom

        focus =
            model.focus
                |> Spring2d.value
                |> Vector2d.mirrorAcross Axis2d.x
                |> Vector2d.components
                |> Frame2d.atCoordinates

        viewBox =
            ( width, height )
                |> Rectangle2d.centeredOn focus
                |> Rectangle2d.topLeftVertex
                |> Point2d.coordinates
                |> (\( left, top ) -> [ left, -top, width, height ])
                |> List.map String.fromFloat
                |> String.join " "
    in
    [ if model.showRegions then
        paintRegions model.world

      else
        Svg.g [] []
    , paintWorld model.world
    ]
        |> Svg.svg
            [ Svg.Attributes.viewBox viewBox
            , Html.Attributes.style "width" "100%"
            , Html.Attributes.style "height" "100%"
            , -- For the SVG / flexbox bug https://github.com/philipwalton/flexbugs/issues/184
              Html.Attributes.style "height" "100vh"
            , Html.Attributes.style "margin-top" "-60px"
            , ( 245, 20, 15 )
                |> Color.fromHSL
                |> Color.toHSLString
                |> Html.Attributes.style "background"
            ]
        |> Element.html
        |> Element.el
            [ Element.width Element.fill
            , Element.height Element.fill
            , trackUi model
                |> Element.el
                    [ Element.alignRight
                    , Element.alignBottom
                    , Element.padding 30
                    , Font.size 12
                    , Font.family [ Font.monospace ]
                    , Font.color (Element.rgb 1 1 1)
                    ]
                |> Element.inFront
            ]


statsUi : Model -> Element Msg
statsUi model =
    let
        dataPoints =
            appendDataPoints model.clock model.stats model.dataPoints

        chartConfig : LineChart.Config DataPoint Msg
        chartConfig =
            { y =
                LineChart.Axis.default 400
                    "Population"
                    .quantity
            , x =
                LineChart.Axis.default 700
                    "Time"
                    (.time >> (\t -> t / 60000))
            , container =
                LineChart.Container.custom
                    { attributesHtml =
                        [ Html.Attributes.style "font-family" "monospace"
                        ]
                    , attributesSvg =
                        [ Html.Attributes.style "font-size" "12px"
                        ]
                    , size = LineChart.Container.relative
                    , margin = LineChart.Container.Margin 30 100 60 80
                    , id = "population-chart"
                    }
            , interpolation = LineChart.Interpolation.monotone
            , intersection = LineChart.Axis.Intersection.default
            , legends =
                LineChart.Legends.byBeginning
                    (LineChart.Junk.label LineChart.Colors.gray)
            , events = LineChart.Events.default
            , junk = LineChart.Junk.default
            , grid = LineChart.Grid.default
            , area = LineChart.Area.default
            , line = LineChart.Line.default
            , dots = LineChart.Dots.default
            }
    in
    [ LineChart.line
        LineChart.Colors.green
        LineChart.Dots.none
        (String.fromFloat model.stats.plants ++ " plants")
        dataPoints.plants
    , LineChart.line
        LineChart.Colors.red
        LineChart.Dots.none
        (String.fromFloat model.stats.herbivores ++ " herbivores")
        dataPoints.herbivores
    , LineChart.line
        LineChart.Colors.gold
        LineChart.Dots.none
        (String.fromFloat model.stats.carnivores ++ " carnivores")
        dataPoints.carnivores
    ]
        |> LineChart.viewCustom chartConfig
        |> Element.html
        |> Element.el
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


controlsUi : Model -> Element Msg
controlsUi model =
    let
        controls =
            [ Input.button
                [ Element.padding 20
                , Font.color
                    (if model.tab == SceneTab then
                        Element.rgb 1 1 1

                     else
                        Element.rgb 0.5 0.5 0.5
                    )
                ]
                { onPress = Just (Show SceneTab)
                , label = Element.text "Scene"
                }
            , Input.button
                [ Element.padding 20
                , Font.color
                    (if model.tab == StatsTab then
                        Element.rgb 1 1 1

                     else
                        Element.rgb 0.5 0.5 0.5
                    )
                ]
                { onPress = Just (Show StatsTab)
                , label = Element.text "Stats"
                }
            , Input.button
                [ Element.padding 20
                , Font.color
                    (if model.tab == SettingsTab then
                        Element.rgb 1 1 1

                     else
                        Element.rgb 0.5 0.5 0.5
                    )
                ]
                { onPress = Just (Show SettingsTab)
                , label = Element.text "Settings"
                }
            , resetButton
            , if model.play then
                pauseButton

              else
                playButton
            , clock
            ]

        resetButton : Element Msg
        resetButton =
            Input.button [ Element.padding 20, Element.alignRight ]
                { onPress = Just Reset
                , label = Element.text "Reset"
                }

        playButton : Element Msg
        playButton =
            Input.button [ Element.padding 20, Element.alignRight ]
                { onPress = Just Play
                , label = Element.text "Play"
                }

        pauseButton : Element Msg
        pauseButton =
            Input.button [ Element.padding 20, Element.alignRight ]
                { onPress = Just Pause
                , label = Element.text "Pause"
                }

        time =
            model.clock
                |> Basics.truncate
                |> Time.millisToPosix

        clock =
            [ time |> Time.toHour Time.utc
            , time |> Time.toMinute Time.utc
            , time |> Time.toSecond Time.utc
            ]
                |> List.map String.fromInt
                |> List.map (String.padLeft 2 '0')
                |> String.join ":"
                |> Element.text
                |> Element.el
                    [ Element.padding 20
                    , Element.alignRight
                    ]
    in
    controls
        |> Element.row
            [ Font.color (Element.rgb 0.5 0.5 0.5)
            , Background.color (Element.rgb 0.2 0.2 0.2)
            , Font.family [ Font.monospace ]
            , Element.width Element.fill
            , Element.spacing 5
            ]



-- Tracking UI


trackUi : Model -> Element Msg
trackUi model =
    case model.track of
        Nothing ->
            Element.none

        Just id ->
            [ id
                |> String.fromInt
                |> Element.text
            , entityStatsUi id model
            ]
                |> Element.row [ Element.spacing 20 ]


entityStatsUi : Int -> Model -> Element Msg
entityStatsUi id model =
    -- Dict.get id model.world.entities
    --     |> Debug.toString
    --     |> Element.text
    --     |> List.singleton
    --     |> Element.paragraph []
    case Dict.get id model.world.entities of
        Nothing ->
            Element.text "Gone"

        Just (Plant this) ->
            plantStatsUi this

        Just (Herbivore this) ->
            herbivoreStatsUi this

        Just (Carnivore this) ->
            carnivoreStatsUi this

        Just entity ->
            Element.text "Not implemented"


herbivoreStatsUi : EntityModels.Herbivore -> Element Msg
herbivoreStatsUi this =
    [ [ "nutrition"
      , this.nutrition
            |> toPrecision 3
            |> String.fromFloat
      ]
    , [ "mass"
      , this.mass
            |> toPrecision 3
            |> String.fromFloat
      ]
    , [ "attraction"
      , this.attraction
            |> Spring2d.value
            |> Vector2d.length
            |> toPrecision 3
            |> String.fromFloat
      ]
    ]
        |> List.map (List.map Element.text >> Element.row [ Element.spacing 20 ])
        |> Element.column [ Element.spacing 20 ]


plantStatsUi : EntityModels.Plant -> Element Msg
plantStatsUi this =
    [ [ "succulence"
      , this.succulence
            |> toPrecision 3
            |> String.fromFloat
      ]
    ]
        |> List.map (List.map Element.text >> Element.row [ Element.spacing 20 ])
        |> Element.column [ Element.spacing 20 ]


carnivoreStatsUi : EntityModels.Carnivore -> Element msg
carnivoreStatsUi this =
    [ [ "nutrition"
      , this.nutrition
            |> toPrecision 3
            |> String.fromFloat
      ]
    , [ "mass"
      , this.mass
            |> toPrecision 3
            |> String.fromFloat
      ]
    , [ "pregnancy"
      , this.pregnancy
            |> Maybe.map (toPrecision 3)
            |> Maybe.map String.fromFloat
            |> Maybe.withDefault "Not pregnant"
      ]
    , [ "attraction"
      , this.attraction
            |> Spring2d.value
            |> Vector2d.length
            |> toPrecision 3
            |> String.fromFloat
      ]
    ]
        |> List.map (List.map Element.text >> Element.row [ Element.spacing 20 ])
        |> Element.column [ Element.spacing 20 ]


toPrecision : Int -> Float -> Float
toPrecision precision number =
    let
        factor =
            precision
                |> toFloat
                |> (^) 10
    in
    number
        * factor
        |> Basics.truncate
        |> toFloat
        |> (\n -> n / factor)



-- SVG elements


paintWorld : World -> Svg Msg
paintWorld world =
    world.entities
        |> Dict.map paintEntity
        |> Dict.values
        |> Svg.g []


paintRegions : World -> Svg Msg
paintRegions world =
    world.index
        |> Dict.keys
        |> List.map paintRegion
        |> Svg.g []


paintRegion : World.Region -> Svg Msg
paintRegion ( x, y ) =
    let
        topLeft =
            Point2d.fromCoordinates
                ( toFloat x * World.regionSize
                , toFloat y * World.regionSize
                )

        bottomRight =
            topLeft
                |> Point2d.translateBy dimensions

        dimensions =
            ( World.regionSize, World.regionSize )
                |> Vector2d.fromComponents
    in
    BoundingBox2d.from topLeft bottomRight
        |> Geometry.Svg.boundingBox2d
            [ Svg.Attributes.strokeDasharray "5 5"
            , Svg.Attributes.stroke "yellow"
            , Svg.Attributes.fill "none"
            , Svg.Attributes.strokeWidth "1"
            ]


paintEntity : Int -> Entity -> Svg Msg
paintEntity id entity =
    Svg.g
        [ Svg.Events.onClick (Track <| Just id)
        , Html.Attributes.style "padding" "10px"
        ]
        [ case entity of
            Seed this ->
                paintSeed this

            Plant this ->
                paintPlant this

            Egg this ->
                paintEgg this

            Herbivore this ->
                paintHerbivore this

            Carnivore this ->
                paintCarnivore this

            Carcass this ->
                paintCarcass this
        ]


paintSeed : EntityModels.Seed -> Svg Msg
paintSeed this =
    let
        size =
            0.1 + Spring.value this.speed / 100
    in
    Svg.circle
        [ Svg.Attributes.r "1"
        , Svg.Attributes.fill "gray"
        , [ Transformations.Translate (Point2d.coordinates this.position)
          , Transformations.Scale ( size, size )
          ]
            |> List.map Transformations.toString
            |> String.join ", "
            |> Svg.Attributes.transform
        ]
        []


paintPlant : EntityModels.Plant -> Svg Msg
paintPlant this =
    let
        size =
            this.succulence
                / 5
                |> max 0.01

        leavesNumber =
            5

        leaves =
            leavesNumber
                |> List.range 1
                |> List.map toFloat
                |> List.map
                    (\index ->
                        Svg.rect
                            [ Svg.Attributes.stroke "darkgreen"
                            , Svg.Attributes.strokeWidth "0.5"
                            , Svg.Attributes.fill "green"
                            , Svg.Attributes.width "1"
                            , Svg.Attributes.height "1"
                            , Svg.Attributes.opacity "0.8"
                            , [ Transformations.Rotate
                                    ((360 / leavesNumber) * index)
                              , Transformations.Scale
                                    ( sqrt size * 3, size * 15 )
                              , Transformations.Rotate 45
                              , Transformations.Translate
                                    ( -0.3, -0.3 )
                              ]
                                |> List.map Transformations.toString
                                |> String.join ", "
                                |> Svg.Attributes.transform
                            ]
                            []
                    )

        flower =
            Svg.rect
                [ Svg.Attributes.stroke "yellow"
                , Svg.Attributes.strokeWidth "1"
                , Svg.Attributes.fill "orange"
                , Svg.Attributes.width "4"
                , Svg.Attributes.height "4"
                , Svg.Attributes.rx "1"
                , Svg.Attributes.ry "2"
                , Svg.Attributes.opacity "0.8"
                , [ Transformations.Scale
                        ( max 0 (size - 0.7)
                        , max 0 (size - 0.7)
                        )
                  , Transformations.Translate ( -2, -2 )
                  ]
                    |> List.map Transformations.toString
                    |> String.join ", "
                    |> Svg.Attributes.transform
                ]
                []

        rotation =
            this.position
                |> Point2d.squaredDistanceFrom Point2d.origin
                |> degrees
    in
    flower
        :: leaves
        |> List.reverse
        |> Svg.g
            [ [ Transformations.Translate (Point2d.coordinates this.position)
              , Transformations.Rotate rotation
              ]
                |> List.map Transformations.toString
                |> String.join ", "
                |> Svg.Attributes.transform
            ]


paintEgg : EntityModels.Egg -> Svg Msg
paintEgg this =
    let
        color =
            Color.fromHSL
                ( 60 - (60 * this.development)
                , 90
                , 60
                )
                |> Color.toHSLString
    in
    Svg.g
        [ this.position
            |> Point2d.coordinates
            |> Transformations.Translate
            |> Transformations.toString
            |> Svg.Attributes.transform
        ]
        [ Svg.circle
            [ Svg.Attributes.stroke "white"
            , Svg.Attributes.strokeWidth "0.3"
            , Svg.Attributes.fill "yellow"
            , Svg.Attributes.r "1.2"
            ]
            []
        , Svg.circle
            [ Svg.Attributes.fill color
            , Svg.Attributes.r "1"
            , ( this.development, this.development )
                |> Transformations.Scale
                |> Transformations.toString
                |> Svg.Attributes.transform
            ]
            []
        ]


paintHerbivore : EntityModels.Herbivore -> Svg Msg
paintHerbivore this =
    let
        size =
            1 + this.mass * 2

        hunger =
            1 - this.nutrition / this.mass

        color =
            Color.fromHSL
                ( 220
                , 90 - (60 * hunger)
                , 60 + (30 * hunger)
                )
    in
    Svg.circle
        [ color
            |> Color.toHSLString
            |> Svg.Attributes.fill
        , color
            |> Color.adjustLightness 20
            |> Color.toHSLString
            |> Svg.Attributes.stroke
        , Svg.Attributes.strokeWidth "0.3"
        , [ Transformations.Translate (Point2d.coordinates this.position)
          , Transformations.Scale ( size, size )
          ]
            |> List.map Transformations.toString
            |> String.join ", "
            |> Svg.Attributes.transform
        , Svg.Attributes.r "1"
        ]
        []


paintCarnivore : EntityModels.Carnivore -> Svg Msg
paintCarnivore this =
    let
        size =
            this.mass
                * 10
                + (this.pregnancy
                    |> Maybe.map ((*) 2)
                    |> Maybe.withDefault 0
                  )

        hunger =
            1 - this.nutrition / this.mass

        color =
            Color.fromHSL
                ( 0
                , 100 - (60 * hunger)
                , 40 + max (60 * hunger) -20
                )

        fill =
            if this.pregnancy /= Nothing then
                Color.fromHSL
                    ( 0
                    , 0
                    , 100
                    )

            else
                Color.adjustLightness 20 color
    in
    Svg.circle
        [ fill
            |> Color.toHSLString
            |> Svg.Attributes.fill
        , color
            |> Color.toHSLString
            |> Svg.Attributes.stroke
        , Svg.Attributes.strokeWidth "0.1"
        , Svg.Attributes.r "0.1"
        , [ Transformations.Translate (Point2d.coordinates this.position)
          , Transformations.Scale ( size, size )
          ]
            |> List.map Transformations.toString
            |> String.join ", "
            |> Svg.Attributes.transform
        ]
        []


paintCarcass : EntityModels.Carcass -> Svg Msg
paintCarcass this =
    let
        size =
            this.mass * 4

        color =
            Color.fromHSL
                ( 0
                , 0
                , 40
                )
    in
    Svg.circle
        [ color
            |> Color.toHSLString
            |> Svg.Attributes.fill
        , [ Transformations.Translate (Point2d.coordinates this.position)
          , Transformations.Scale ( size, size )
          ]
            |> List.map Transformations.toString
            |> String.join ", "
            |> Svg.Attributes.transform
        , Svg.Attributes.r "1"
        ]
        []
