module Settings exposing
    ( Msg
    , Settings
    , defaults
    , ui
    , update
    )

import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input


type alias Settings =
    { -- Carcass
      decompositionRate : Float

    -- Plants and Seed
    , seedsCentripetalAttraction : Float
    , seedingVegetationLimit : Float
    , germinationVegetationLimit : Float
    , seedingMinSucculence : Float
    , plantGrowthRate : Float
    , seedingCost : Float
    , seedAttractionDampness : Float
    , seedAttractionStrength : Float
    , seedInitialSpeed : Float
    , seedSpeedStrength : Float
    , seedSpeedDampness : Float

    -- Herbivores and Eggs
    , eggDevelopmentRate : Float
    , herbivoreOvulationMinNutrition : Float
    , herbivoreFoodAttraction : Float
    , herbivoreMutualRepulsion : Float
    , herbivoreEggRepulsion : Float
    , herbivoreCarnivoreRepulsion : Float
    , herbivoreConsumptionThershold : Float
    , herbivoreIdleThershold : Float
    , herbivoreMoveSpeed : Float
    , herbivoreMoveCost : Float
    , herbivoreBirthMass : Float
    , herbivoreBirthNutrition : Float
    , herbivoreAttractionStrength : Float
    , herbivoreAttractionDampness : Float
    , herbivoreOvulationCost : Float
    , herbivoreConsumptionRate : Float
    , herbivoreConsumptionEfficiency : Float
    , herbivoreGrowthRate : Float
    , herbivoreGrowthCost : Float

    -- Carnivores
    , carnivoreCarcassAttraction : Float
    , carnivorePreyAttraction : Float
    , carnivoreSeedAttraction : Float
    , carnivoreSexualAttraction : Float
    , carnivorePregnancyHuntingBoost : Float
    , carnivoreMatingMinNutrition : Float
    , carnivoreMaturityMinMass : Float
    , carnivoreMutualRepulsion : Float
    , carnivoreConsumptionThreshold : Float
    , carnivoreIdleThreshold : Float
    , carnivoreGrowthRate : Float
    , carnivoreGrowthCost : Float
    , carnivoreMoveSpeed : Float
    , carnivoreMoveCost : Float
    , carnivoreMatingCost : Float
    , carnivorePregnancyDevelopmentRate : Float
    , carnivorePregnancyCost : Float
    , carnivoreBirthCost : Float
    , carnivoreConsumptionRate : Float
    , carnivoreConsumptionEfficiency : Float
    , carnivoreSeedConsumptionGain : Float
    , carnivoreBirthMass : Float
    , carnivoreBirthNutrition : Float
    , carnivoreAttractionStrength : Float
    , carnivoreAttractionDampness : Float
    , carnivoreAttackDamage : Float
    , carnivoreAttackCost : Float
    }


defaults : Settings
defaults =
    { -- Carcass
      decompositionRate = 0.005

    -- Plants and Seed
    , seedsCentripetalAttraction = 0.001
    , seedingVegetationLimit = 5
    , germinationVegetationLimit = 10
    , seedingMinSucculence = 5
    , plantGrowthRate = 0.025
    , seedingCost = 1
    , seedAttractionDampness = 3
    , seedAttractionStrength = 10
    , seedInitialSpeed = 100
    , seedSpeedStrength = 2
    , seedSpeedDampness = 2

    -- Herbivores and Eggs
    , eggDevelopmentRate = 0.02
    , herbivoreOvulationMinNutrition = 1
    , herbivoreFoodAttraction = 5
    , herbivoreMutualRepulsion = 2
    , herbivoreEggRepulsion = 20
    , herbivoreCarnivoreRepulsion = 150
    , herbivoreConsumptionThershold = 150
    , herbivoreIdleThershold = 0.5
    , herbivoreMoveSpeed = 10
    , herbivoreMoveCost = 0.0006
    , herbivoreBirthMass = 0.1
    , herbivoreBirthNutrition = 0.1
    , herbivoreAttractionStrength = 40
    , herbivoreAttractionDampness = 4
    , herbivoreOvulationCost = 0.5
    , herbivoreConsumptionRate = 1
    , herbivoreConsumptionEfficiency = 0.25
    , herbivoreGrowthRate = 0.003
    , herbivoreGrowthCost = 2

    -- Carnivores
    , carnivoreCarcassAttraction = 5
    , carnivorePreyAttraction = 3
    , carnivoreSeedAttraction = 0.1
    , carnivoreSexualAttraction = 5
    , carnivorePregnancyHuntingBoost = 10
    , carnivoreMatingMinNutrition = 1
    , carnivoreMaturityMinMass = 1
    , carnivoreMutualRepulsion = 3
    , carnivoreConsumptionThreshold = 150
    , carnivoreIdleThreshold = 0.02
    , carnivoreGrowthRate = 0.003
    , carnivoreGrowthCost = 1
    , carnivoreMoveSpeed = 20
    , carnivoreMoveCost = 0.0005
    , carnivoreMatingCost = 0.2
    , carnivorePregnancyDevelopmentRate = 0.01
    , carnivorePregnancyCost = 0.5
    , carnivoreBirthCost = 0.3
    , carnivoreConsumptionRate = 0.05
    , carnivoreConsumptionEfficiency = 0.8
    , carnivoreSeedConsumptionGain = 0.05
    , carnivoreBirthMass = 0.4
    , carnivoreBirthNutrition = 0.4
    , carnivoreAttractionStrength = 500
    , carnivoreAttractionDampness = 5
    , carnivoreAttackDamage = 0.2
    , carnivoreAttackCost = 0.01
    }


type
    Msg
    -- Plants and Seed
    = SetSeedsCentripetalAttraction Float
    | SetStemmingVegetationLimit Float
    | SetGerminationVegetationLimit Float
    | SetGerminationMinSucculence Float
      -- Herbivores and Eggs
    | SetEggDevelopmentRate Float
    | SetHerbivoreOvulationMinNutrition Float
    | SetHerbivoreFoodAttraction Float
    | SetHerbivoreMutualRepulsion Float
    | SetHerbivoreEggRepulsion Float
    | SetHerbivoreCarnivoreRepulsion Float
    | SetHerbivoreConsumptionThershold Float
    | SetHerbivoreIdleThershold Float
      -- Carnivores
    | SetCarnivorePreyAttraction Float
    | SetCarnivoreSeedAttraction Float
    | SetCarnivorePregnancyHuntingBoost Float
    | SetCarnivoreMatingMinNutrition Float
    | SetCarnivoreMaturityMinMass Float
    | SetCarnivoreMutualRepulsion Float
    | SetCarnivoreConsumptionThreshold Float
    | SetCarnivoreIdleThreshold Float


update : Msg -> Settings -> Settings
update msg settings =
    case msg of
        -- Plants and Seed
        SetSeedsCentripetalAttraction value ->
            { settings | seedsCentripetalAttraction = value }

        SetStemmingVegetationLimit value ->
            { settings | seedingVegetationLimit = value }

        SetGerminationVegetationLimit value ->
            { settings | germinationVegetationLimit = value }

        SetGerminationMinSucculence value ->
            { settings | seedingMinSucculence = value }

        -- Herbivores and Eggs
        SetEggDevelopmentRate value ->
            { settings | eggDevelopmentRate = value }

        SetHerbivoreOvulationMinNutrition value ->
            { settings | herbivoreOvulationMinNutrition = value }

        SetHerbivoreFoodAttraction value ->
            { settings | herbivoreFoodAttraction = value }

        SetHerbivoreMutualRepulsion value ->
            { settings | herbivoreMutualRepulsion = value }

        SetHerbivoreEggRepulsion value ->
            { settings | herbivoreEggRepulsion = value }

        SetHerbivoreCarnivoreRepulsion value ->
            { settings | herbivoreCarnivoreRepulsion = value }

        SetHerbivoreConsumptionThershold value ->
            { settings | herbivoreConsumptionThershold = value }

        SetHerbivoreIdleThershold value ->
            { settings | herbivoreIdleThershold = value }

        -- Carnivores
        SetCarnivorePreyAttraction value ->
            { settings | carnivorePreyAttraction = value }

        SetCarnivoreSeedAttraction value ->
            { settings | carnivoreSeedAttraction = value }

        SetCarnivorePregnancyHuntingBoost value ->
            { settings | carnivorePregnancyHuntingBoost = value }

        SetCarnivoreMatingMinNutrition value ->
            { settings | carnivoreMatingMinNutrition = value }

        SetCarnivoreMaturityMinMass value ->
            { settings | carnivoreMaturityMinMass = value }

        SetCarnivoreMutualRepulsion value ->
            { settings | carnivoreMutualRepulsion = value }

        SetCarnivoreConsumptionThreshold value ->
            { settings | carnivoreConsumptionThreshold = value }

        SetCarnivoreIdleThreshold value ->
            { settings | carnivoreIdleThreshold = value }


ui :
    { onChange : Msg -> msg
    , onSubmit : Settings -> msg
    }
    -> Settings
    -> Element msg
ui { onChange, onSubmit } settings =
    let
        sliderUI :
            { onChange : Float -> Msg
            , label : String
            , min : Float
            , max : Float
            , value : Float
            , step : Float
            }
            -> Element Msg
        sliderUI input =
            Input.slider
                [ Element.behindContent
                    (Element.el
                        [ Element.width Element.fill
                        , Element.height (Element.px 2)
                        , Element.centerY
                        , Background.color <| Element.rgb 0.7 0.7 0.7
                        , Border.rounded 2
                        ]
                        Element.none
                    )
                ]
                { onChange = input.onChange
                , label =
                    Input.labelAbove []
                        ([ input.label
                         , ": "
                         , String.fromFloat input.value
                         ]
                            |> String.concat
                            |> Element.text
                        )
                , min = input.min
                , max = input.max
                , value = input.value
                , thumb = Input.defaultThumb
                , step = Just input.step
                }
    in
    [ [ -- Plants and Seed
        { label = "Seeds centripetal attraction"
        , onChange = SetSeedsCentripetalAttraction
        , min = 0
        , max = 0.1
        , value = settings.seedsCentripetalAttraction
        , step = 0.001
        }
      , { label = "Vegetation limit for plants seeding "
        , onChange = SetStemmingVegetationLimit
        , min = 0
        , max = 100
        , value = settings.seedingVegetationLimit
        , step = 1
        }
      , { label = "Vegetation limit for seeds germination"
        , onChange = SetGerminationVegetationLimit
        , min = 0
        , max = 100
        , value = settings.germinationVegetationLimit
        , step = 1
        }
      , { label = "Minimal succulence for plants seeding"
        , onChange = SetGerminationMinSucculence
        , min = 0
        , max = 10
        , value = settings.seedingMinSucculence
        , step = 0.1
        }

      -- Herbivores and Eggs
      , { label = "Egg hatching development"
        , onChange = SetEggDevelopmentRate
        , min = 0
        , max = 10
        , value = settings.eggDevelopmentRate
        , step = 0.1
        }
      , { label = "Minimal nutrition for herbivores ovulation"
        , onChange = SetHerbivoreOvulationMinNutrition
        , min = 0
        , max = 10
        , value = settings.herbivoreOvulationMinNutrition
        , step = 0.1
        }
      , { label = "Food attraction factor for herbivores"
        , onChange = SetHerbivoreFoodAttraction
        , min = 0
        , max = 200
        , value = settings.herbivoreFoodAttraction
        , step = 2
        }
      , { label = "Mutual repulsion factor for herbivores"
        , onChange = SetHerbivoreMutualRepulsion
        , min = 0
        , max = 200
        , value = settings.herbivoreMutualRepulsion
        , step = 2
        }
      , { label = "Egg repulsion factor for herbivores"
        , onChange = SetHerbivoreEggRepulsion
        , min = 0
        , max = 200
        , value = settings.herbivoreEggRepulsion
        , step = 2
        }
      , { label = "Carnivore repulsion factor for herbivores"
        , onChange = SetHerbivoreCarnivoreRepulsion
        , min = 0
        , max = 200
        , value = settings.herbivoreCarnivoreRepulsion
        , step = 2
        }
      , { label = "Consuming herbivores move threshold"
        , onChange = SetHerbivoreConsumptionThershold
        , min = 0
        , max = 200
        , value = settings.herbivoreConsumptionThershold
        , step = 2
        }
      , { label = "Idle herbivore move threshold"
        , onChange = SetHerbivoreIdleThershold
        , min = 0
        , max = 200
        , value = settings.herbivoreIdleThershold
        , step = 2
        }

      -- Carnivores
      , { label = "Prey attraction factor for carnivores"
        , onChange = SetCarnivorePreyAttraction
        , min = 0
        , max = 200
        , value = settings.carnivorePreyAttraction
        , step = 2
        }
      , { label = "Seed attraction factor for carnivores"
        , onChange = SetCarnivoreSeedAttraction
        , min = 0
        , max = 200
        , value = settings.carnivoreSeedAttraction
        , step = 2
        }
      , { label = "Hunting boost for pregnant carnivores"
        , onChange = SetCarnivorePregnancyHuntingBoost
        , min = 0
        , max = 100
        , value = settings.carnivorePregnancyHuntingBoost
        , step = 1
        }
      , { label = "Minimal nutrition for carnivores mating"
        , onChange = SetCarnivoreMatingMinNutrition
        , min = 0
        , max = 100
        , value = settings.carnivoreMatingMinNutrition
        , step = 1
        }
      , { label = "Minimal mass for carnivore maturity "
        , onChange = SetCarnivoreMaturityMinMass
        , min = 0
        , max = 10
        , value = settings.carnivoreMaturityMinMass
        , step = 0.1
        }
      , { label = "Mutual repulsion factor for carnivores"
        , onChange = SetCarnivoreMutualRepulsion
        , min = 0
        , max = 200
        , value = settings.carnivoreMutualRepulsion
        , step = 2
        }
      , { label = "Consuming carnivore move threshold"
        , onChange = SetCarnivoreConsumptionThreshold
        , min = 0
        , max = 200
        , value = settings.carnivoreConsumptionThreshold
        , step = 2
        }
      , { label = "Idle carnivore move threshold"
        , onChange = SetCarnivoreIdleThreshold
        , min = 0
        , max = 5
        , value = settings.carnivoreIdleThreshold
        , step = 0.02
        }
      ]
        |> List.map sliderUI
        |> Element.column
            [ Element.spacing 20
            , Element.width Element.fill
            ]
        |> Element.map onChange
    , Input.button []
        { label = "Submit" |> Element.text
        , onPress = Just (onSubmit settings)
        }
    ]
        |> Element.column
            [ Element.spacing 20
            , Element.padding 20
            , Element.width Element.fill
            ]



--     {
--     -- Seeds
--     seedSpeedSpring = { strength = 2, dampness = 2 }
--     , seedInitialSpeed = 100
--     , seedAttractionSpring = { strength = 3, dampness = 3 }
--     , seedVegitationLimit = 7
--     , seedNutritionCost = 0.3
--     -- Eggs
--     , eggDevelopmentRatio = 0
--     -- plants
--     , plantSucculence = 0
--     , plantVegitationLimit = 14
--     }
--
--
--
--     }
--
--   =
--     { herbivoreBirthMassRatio = 0.1
--     , herbivoreBirthNutritionRatio = 0.1
--     , herbivoreAttraction = Spring2d.create { strength = 20, dampness = 4 }
--     }
--
-- prototypes :
--     { seed : EntityModels.Seed
--     , plant : EntityModels.Plant
--     , egg : EntityModels.Egg
--     , herbivore : EntityModels.Herbivore
--     , carnivore : EntityModels.Carnivore
--     , carcass : EntityModels.Carcass
--     }
-- prototypes =
--     { seed =
--         { position = Point2d.origin
--         , speed =
--             { strength = 2, dampness = 2 }
--                 |> Spring.create
--                 |> Spring.jumpTo 100
--                 |> Spring.setTarget 0
--         , attraction =
--             { strength = 3, dampness = 3 }
--                 |> Spring2d.create
--         }
--     , plant =
--         { position = Point2d.origin
--         , succulence = 0
--         }
--     , egg =
--         { position = Point2d.origin
--         , development = 0
--         }
--     , herbivore =
--         { position = Point2d.origin
--         , mass = 0.1
--         , nutrition = 0.1
--         , attraction = Spring2d.create { strength = 20, dampness = 4 }
--         }
--     , carnivore =
--         { position = Point2d.origin
--         , mass = 0.1
--         , nutrition = 1.0
--         , pregnancy = Nothing
--         , attraction =
--             Spring2d.create
--                 { strength = 10
--                 , dampness = 5
--                 }
--         }
--     , carcass =
--         { position = Point2d.origin
--         , mass = 1
--         }
--     }
