module Spring2d exposing
    ( Spring2d
    , animate
    , atRest
    , create
    , jumpTo
    , setTarget
    , target
    , value
    )

import Spring exposing (Spring)
import Vector2d exposing (Vector2d)


type alias Spring2d =
    { x : Spring
    , y : Spring
    }


create : { strength : Basics.Float, dampness : Basics.Float } -> Spring2d
create config =
    { x = Spring.create config
    , y = Spring.create config
    }


animate : Float -> Spring2d -> Spring2d
animate delta { x, y } =
    { x = Spring.animate delta x
    , y = Spring.animate delta y
    }


setTarget : Vector2d -> Spring2d -> Spring2d
setTarget vector { x, y } =
    { x = Spring.setTarget (Vector2d.xComponent vector) x
    , y = Spring.setTarget (Vector2d.yComponent vector) y
    }


jumpTo : Vector2d -> Spring2d -> Spring2d
jumpTo vector { x, y } =
    { x = Spring.jumpTo (Vector2d.xComponent vector) x
    , y = Spring.jumpTo (Vector2d.yComponent vector) y
    }


atRest : Spring2d -> Bool
atRest { x, y } =
    Spring.atRest x && Spring.atRest y


value : Spring2d -> Vector2d
value { x, y } =
    ( x, y )
        |> Tuple.mapBoth Spring.value Spring.value
        |> Vector2d.fromComponents


target : Spring2d -> Vector2d
target { x, y } =
    ( x, y )
        |> Tuple.mapBoth Spring.target Spring.target
        |> Vector2d.fromComponents
