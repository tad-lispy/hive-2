import { Elm  } from "./Main.elm"

const node = document.getElementById("app-container")
const program = Elm.Main

const flags = {
  world : JSON.parse(localStorage.getItem("snapshot"))
}


const main = program.init({ node, flags });
const worker = new Worker("worker.js");

// Worker is responsible for calculating intentions for all entities.
worker.onmessage = (event) => {
  // console.count("intentions in")
  // const message = event.data;
  // console.log(`[From Worker]: ${message}`);

  // console.time("intentions update")
  main.ports.intentionsIn.send(event.data)
};

main.ports.worldOut.subscribe((data) => {
  // console.count("New world update")
  worker.postMessage(data)
  // console.log("Message sent");

})

main.ports.errorsOut.subscribe(({ message, details }) => {
  // console.error(message, details)
})

main.ports.snapshotsOut.subscribe((world) => {
  localStorage.setItem("snapshot", JSON.stringify(world))
})

main.ports.resetOut.subscribe(() => {
  // console.log("reset");
  localStorage.removeItem("snapshot")
  location.reload()
})


console.log("Main thread ready");
