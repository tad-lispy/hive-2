module IntentionsWorker exposing (main)

import Intentions exposing (Intentions)
import Json.Decode as Decode
import Json.Encode as Encode
import Ports
import World exposing (World)


main : Program Flags Intentions Msg
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    ()


type Msg
    = WorldUpdate (Result Decode.Error World)


init : Flags -> ( Intentions, Cmd Msg )
init _ =
    ( Intentions.calculate World.empty
    , Cmd.none
    )


update : Msg -> Intentions -> ( Intentions, Cmd Msg )
update msg old =
    case msg of
        WorldUpdate (Ok world) ->
            let
                intentions =
                    Intentions.calculate world
            in
            ( intentions
            , Ports.sendIntentions intentions
            )

        WorldUpdate (Err error) ->
            ( old
            , error
                |> Decode.errorToString
                |> Encode.string
                |> Just
                |> Ports.sendError "Error decoding intentions"
            )


subscriptions : Intentions -> Sub Msg
subscriptions intentions =
    Ports.world WorldUpdate
