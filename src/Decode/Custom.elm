module Decode.Custom exposing
    ( carcass
    , carnivore
    , egg
    , entities
    , entity
    , herbivore
    , intentions
    , plant
    , point2d
    , seed
    , spring
    , spring2d
    , vector2d
    , world
    )

import Dict
import Direction2d
import EntityModels
import Intentions exposing (Action(..), Intention, Intentions)
import Json.Decode as Decode exposing (..)
import Json.Decode.Extra as Decode exposing (..)
import Json.Decode.Pipeline exposing (..)
import Point2d exposing (Point2d)
import Result.Extra as Result
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)
import Vector2d exposing (Vector2d)
import World exposing (Entities, Entity(..), World)


world : Decoder World
world =
    map World.restore (field "entities" entities)


intentions : Decoder Intentions
intentions =
    let
        entry ( key, data ) =
            case String.toInt key of
                Just id ->
                    Ok ( id, data )

                Nothing ->
                    Err ("Can't convert key (\"" ++ key ++ "\" : String) to id (Int)")

        entries :
            List ( String, Intention )
            -> Decoder (List ( Int, Intention ))
        entries data =
            data
                |> List.map entry
                |> Result.combine
                |> Decode.fromResult
    in
    keyValuePairs intention
        |> andThen entries
        |> map Dict.fromList


intention : Decoder Intention
intention =
    map2 Intention
        (field "attraction" vector2d)
        (field "action" action)


action : Decoder Action
action =
    field "tag" string
        |> andThen
            (\tag ->
                case tag of
                    "Idle" ->
                        succeed Idle

                    "Move" ->
                        succeed Move
                            |> required "direction" (float |> map Direction2d.fromAngle)

                    "Consume" ->
                        succeed Consume
                            |> required "id" int

                    "Mate" ->
                        succeed Mate
                            |> required "id" int

                    "Hatch" ->
                        succeed Hatch

                    "Germinate" ->
                        succeed Germinate

                    "ScatterSeed" ->
                        succeed ScatterSeed

                    _ ->
                        fail ("Unrecognized action tag: " ++ tag)
            )


entities : Decoder Entities
entities =
    let
        entry ( key, data ) =
            case String.toInt key of
                Just id ->
                    Ok ( id, data )

                Nothing ->
                    Err ("Can't convert key (\"" ++ key ++ "\" : String) to id (Int)")

        entries :
            List ( String, Entity )
            -> Decoder (List ( Int, Entity ))
        entries data =
            data
                |> List.map entry
                |> Result.combine
                |> Decode.fromResult
    in
    keyValuePairs entity
        |> andThen entries
        |> map Dict.fromList


entity : Decoder Entity
entity =
    field "tag" string
        |> andThen
            (\tag ->
                case tag of
                    "Seed" ->
                        seed

                    "Plant" ->
                        plant

                    "Egg" ->
                        egg

                    "Herbivore" ->
                        herbivore

                    "Carnivore" ->
                        carnivore

                    "Carcass" ->
                        carcass

                    _ ->
                        fail ("Unrecognized entity tag: " ++ tag)
            )


seed : Decoder Entity
seed =
    succeed EntityModels.Seed
        |> required "position" point2d
        |> required "attraction" (spring2d { strength = 3, dampness = 3 })
        |> required "speed" (spring { strength = 2, dampness = 2 })
        |> map Seed


plant : Decoder Entity
plant =
    succeed EntityModels.Plant
        |> required "position" point2d
        |> required "succulence" float
        |> map Plant


egg : Decoder Entity
egg =
    succeed EntityModels.Egg
        |> required "position" point2d
        |> required "development" float
        |> map Egg


herbivore : Decoder Entity
herbivore =
    succeed EntityModels.Herbivore
        |> required "position" point2d
        |> required "mass" float
        |> required "nutrition" float
        |> required "attraction"
            (spring2d
                { strength = 40
                , dampness = 2
                }
            )
        |> map Herbivore


carnivore : Decoder Entity
carnivore =
    succeed EntityModels.Carnivore
        |> required "position" point2d
        |> required "mass" float
        |> required "nutrition" float
        |> required "pregnancy" (nullable float)
        |> required "attraction" (spring2d { strength = 10, dampness = 5 })
        |> map Carnivore


carcass : Decoder Entity
carcass =
    succeed EntityModels.Carcass
        |> required "position" point2d
        |> required "mass" float
        |> map Carcass


point2d : Decoder Point2d
point2d =
    Decode.map2 Tuple.pair
        (field "x" float)
        (field "y" float)
        |> Decode.map Point2d.fromCoordinates


vector2d : Decoder Vector2d
vector2d =
    Decode.map2 Tuple.pair
        (field "x" float)
        (field "y" float)
        |> Decode.map Vector2d.fromComponents


spring2d : { strength : Float, dampness : Float } -> Decoder Spring2d
spring2d config =
    map2
        (\target value ->
            Spring2d.create config
                |> Spring2d.jumpTo value
                |> Spring2d.setTarget target
        )
        (field "target" vector2d)
        (field "value" vector2d)


spring : { strength : Float, dampness : Float } -> Decoder Spring
spring config =
    map2
        (\target value ->
            Spring.create config
                |> Spring.jumpTo value
                |> Spring.setTarget target
        )
        (field "target" float)
        (field "value" float)
