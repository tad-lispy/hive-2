module World exposing
    ( Entities
    , Entity(..)
    , Region
    , World
    , empty
    , insert
    , neighbouringRegions
    , populateCarnivores
    , populateHerbivores
    , populatePlants
    , position
    , region
    , regionSize
    , remove
    , replace
    , restore
    )

import Dict exposing (Dict)
import EntityModels
import List.Extra as List
import Point2d exposing (Point2d)
import Settings exposing (Settings)
import Spring exposing (Spring)
import Spring2d exposing (Spring2d)


type alias World =
    { seed : Int
    , entities : Entities
    , index : GeoIndex
    , settings : Settings
    }


type alias Entities =
    Dict Int Entity


type Entity
    = Seed EntityModels.Seed
    | Plant EntityModels.Plant
    | Egg EntityModels.Egg
    | Herbivore EntityModels.Herbivore
    | Carnivore EntityModels.Carnivore
    | Carcass EntityModels.Carcass


empty : World
empty =
    { seed = 0
    , entities = Dict.empty
    , index = Dict.empty
    , settings = Settings.defaults
    }


restore : Entities -> World
restore entities =
    entities
        |> Dict.foldl
            (\id entity world ->
                { world
                    | entities =
                        Dict.insert id entity world.entities
                    , seed = id + 1
                    , index = register (region entity) id world.index
                }
            )
            empty


insert : Entity -> World -> World
insert entity world =
    { world
        | seed = world.seed + 1
        , entities =
            world.entities
                |> Dict.insert world.seed entity
        , index =
            world.index
                |> register (region entity) world.seed
    }


replace : Int -> Entity -> World -> World
replace id entity world =
    case Dict.get id world.entities of
        Nothing ->
            -- Can't replace Nothing
            world

        Just previous ->
            { world
                | entities =
                    world.entities
                        |> Dict.update id (Maybe.map (always entity))
                , index =
                    if region previous == region entity then
                        world.index

                    else
                        world.index
                            |> deRegister (region previous) id
                            |> register (region entity) id
            }


remove : Int -> World -> World
remove id world =
    case Dict.get id world.entities of
        Nothing ->
            -- Can't replace Nothing
            world

        Just entity ->
            { world
                | entities =
                    world.entities
                        |> Dict.remove id
                , index =
                    world.index
                        |> deRegister (region entity) id
            }


populatePlants : Int -> World -> World
populatePlants count world =
    count
        |> List.range 1
        |> List.map toFloat
        |> List.map
            (\index ->
                Plant
                    { position = Point2d.fromPolarCoordinates ( index * 12, index )
                    , succulence = 5
                    }
            )
        |> List.foldl insert world


populateHerbivores : Int -> World -> World
populateHerbivores count world =
    count
        |> List.range 1
        |> List.map toFloat
        |> List.map
            (\index ->
                Herbivore
                    { position =
                        ( index * 10, index )
                            |> Point2d.fromPolarCoordinates
                    , mass = 1.0
                    , nutrition = 1.0
                    , attraction =
                        Spring2d.create
                            { strength = world.settings.herbivoreAttractionStrength
                            , dampness = world.settings.herbivoreAttractionDampness
                            }
                    }
            )
        |> List.foldl insert world


populateCarnivores : Int -> World -> World
populateCarnivores count world =
    count
        |> List.range 1
        |> List.map toFloat
        |> List.map
            (\index ->
                Carnivore
                    { position =
                        ( index * 50, index )
                            |> Point2d.fromPolarCoordinates
                    , mass = 1
                    , nutrition = 1
                    , pregnancy = Nothing
                    , attraction =
                        Spring2d.create
                            { strength = world.settings.carnivoreAttractionStrength
                            , dampness = world.settings.carnivoreAttractionDampness
                            }
                    }
            )
        |> List.foldl insert world


position : Entity -> Point2d
position entity =
    case entity of
        Seed this ->
            this.position

        Plant this ->
            this.position

        Egg this ->
            this.position

        Herbivore this ->
            this.position

        Carnivore this ->
            this.position

        Carcass this ->
            this.position



-- Geo Index


type alias GeoIndex =
    Dict Region (List Int)


type alias Region =
    ( Int, Int )


regionSize : Float
regionSize =
    50


register : Region -> Int -> GeoIndex -> GeoIndex
register key id index =
    Dict.update key (indexInsert id) index


deRegister : Region -> Int -> GeoIndex -> GeoIndex
deRegister key id index =
    Dict.update key (indexRemove id) index


indexInsert : Int -> Maybe (List Int) -> Maybe (List Int)
indexInsert id record =
    record
        |> Maybe.withDefault []
        |> (::) id
        |> Just


indexRemove : Int -> Maybe (List Int) -> Maybe (List Int)
indexRemove id record =
    record
        |> Maybe.map (List.filter ((/=) id))
        |> Maybe.andThen
            (\ids ->
                case List.filter ((/=) id) ids of
                    [] ->
                        -- If it was the only item in the record, remove the record
                        Nothing

                    rest ->
                        Just rest
            )


region : Entity -> Region
region entity =
    entity
        |> position
        |> Point2d.coordinates
        |> Tuple.mapBoth
            (\x -> floor (x / regionSize))
            (\y -> floor (y / regionSize))


neighbouringRegions : Int -> Region -> List Region
neighbouringRegions span ( x, y ) =
    let
        xs =
            List.range (x - span) (x + span)

        ys =
            List.range (y - span) (y + span)
    in
    List.lift2 Tuple.pair xs ys
