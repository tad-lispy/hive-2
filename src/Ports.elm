port module Ports exposing
    ( intentions
    , reset
    , sendError
    , sendIntentions
    , sendSnapshot
    , sendWorld
    , world
    )

import Decode.Custom as Decode
import Encode.Custom as Encode
import Intentions exposing (Intentions)
import Json.Decode as Decode
import Json.Encode as Encode
import World exposing (World)


port snapshotsOut : Encode.Value -> Cmd msg


port worldIn : (Encode.Value -> msg) -> Sub msg


port worldOut : Encode.Value -> Cmd msg


port intentionsIn : (Encode.Value -> msg) -> Sub msg


port intentionsOut : Encode.Value -> Cmd msg


port errorsOut : Encode.Value -> Cmd msg


port resetOut : Encode.Value -> Cmd msg


sendSnapshot : World -> Cmd msg
sendSnapshot data =
    data
        |> Encode.world
        |> snapshotsOut


sendWorld : World -> Cmd msg
sendWorld data =
    data
        |> Encode.world
        |> worldOut


sendIntentions : Intentions -> Cmd msg
sendIntentions data =
    data
        |> Encode.intentions
        |> intentionsOut


intentions : (Result Decode.Error Intentions -> msg) -> Sub msg
intentions tagger =
    intentionsIn (Decode.decodeValue Decode.intentions)
        |> Sub.map tagger


world : (Result Decode.Error World -> msg) -> Sub msg
world tagger =
    worldIn (Decode.decodeValue Decode.world)
        |> Sub.map tagger


sendError : String -> Maybe Encode.Value -> Cmd msg
sendError message details =
    Encode.object
        [ ( "message", message |> Encode.string )
        , ( "details"
          , details
                |> Maybe.withDefault Encode.null
          )
        ]
        |> errorsOut


reset : Cmd msg
reset =
    Encode.null |> resetOut
